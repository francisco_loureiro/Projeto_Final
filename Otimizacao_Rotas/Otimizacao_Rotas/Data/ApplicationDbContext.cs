﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Otimizacao_Rotas.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otimizacao_Rotas.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Order> order { get; set; }
       public DbSet<Client> client { get; set; }


    }
}
