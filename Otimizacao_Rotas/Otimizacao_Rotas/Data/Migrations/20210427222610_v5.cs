﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Otimizacao_Rotas.Data.Migrations
{
    public partial class v5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PhoneNumer",
                table: "client",
                newName: "PhoneNumber");

            migrationBuilder.RenameColumn(
                name: "Adress",
                table: "client",
                newName: "Address");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PhoneNumber",
                table: "client",
                newName: "PhoneNumer");

            migrationBuilder.RenameColumn(
                name: "Address",
                table: "client",
                newName: "Adress");
        }
    }
}
