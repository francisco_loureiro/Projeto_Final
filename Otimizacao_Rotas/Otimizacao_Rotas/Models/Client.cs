﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Otimizacao_Rotas.Models
{
    public class Client
    {
        public int Id { get; set; }


        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "UserName")]
        public string Name { get; set; }


        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }


        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Address")]
        public string Address { get; set; }


        [Required]
        [RegularExpression("^\\d{4}-\\d{3}$")]
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(160)]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(160)]
        [Display(Name = "District")]
        public string District { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(160)]
        [Display(Name = "Country")]
        public string Country { get; set; }


        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number")]
        public int PhoneNumber { get; set; }

        
        


        

    }
}
