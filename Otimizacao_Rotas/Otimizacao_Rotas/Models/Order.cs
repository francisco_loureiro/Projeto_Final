﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Otimizacao_Rotas.Models
{
    public class Order
    {
        [Display(Name = "OrderNumber")]
        public int Id { get; set; }

        public int ClientId { get; set; }

        public string Adress { get; set; }
        

        public DateTime date { get; set; }

        public string CourierId { get; set; }

        public bool Delivery { get; set; }

    }
}
