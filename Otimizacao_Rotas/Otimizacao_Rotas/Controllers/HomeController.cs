﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otimizacao_Rotas.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Otimizacao_Rotas.Data;
using Otimizacao_Rotas.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;

namespace Otimizacao_Rotas.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext context, UserManager<IdentityUser> userManager)
        {
            _logger = logger;
            _context  =  context;
            _userManager = userManager;

        }
        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Authorize(Roles ="Admin")]
        //[AllowAnonymous]
        public IActionResult AddOrder()
        {
            ViewBag.fds  =  1;
            List<OrderClient> orderclientList = new List<OrderClient>();

            List<Client> clients = new List<Client>();
            clients = _context.client.ToList();
            var intArray = new int[clients.Count];
            int i  =  0;

            //var usersWithRoles = (from user in _context.Users
            //                      select new
            //                      {
            //                          CourierId = user.Id,
            //                          CourierName = user.UserName,

            //                          //RoleNames = (from userRole in user.Roles
            //                          //             join role in context.Roles on userRole.RoleId
            //                          //             equals role.Id
            //                          //             select role.Name).ToList()
            //                      }).ToList().Select(p => new OrderClient()

            //                      {
            //                          CourierId = int.Parse(p.CourierId),
            //                          CourierName = p.CourierName,
            //                          //Role = string.Join(",", p.RoleNames)
            //                      });



            var rolesCourier = _context.Roles.FirstOrDefault(y => y.Name == "Courier").Id.ToString();
            var CouriersId = _context.UserRoles.Where(x => x.RoleId == rolesCourier).Select(y => y.UserId).ToList();

            //if (CouriersId != null)
            //{
            //    //Adicionar encomendas numa lista para a view
            //    List<Order> TdsOrders = new List<Order>();
            //    TdsOrders = _context.order.ToList();
            //    TdsOrders = TdsOrders.OrderBy(x => x.date).ToList();
            //    foreach (var item in TdsOrders)
            //    {
            //        if (item.CourierId == idCourier && DateTime.Compare(item.date, date) < 0)
            //        {

            //            ViewBag.DatasEncomendas = item.date;
            //        }

            //    }
            //}



            //Add Order
            foreach (var item in _context.Users)
            {

               
                OrderClient orderclient = new OrderClient();
                if(CouriersId.Contains(item.Id))
                {
                    orderclient.CourierId = item.Id;

                    orderclient.CourierName = item.UserName;

                    orderclientList.Add(orderclient);
                }
                
            }

            
            foreach (var item in clients)
            {

                OrderClient orderclient = new OrderClient();

                orderclient.Name = item.Name;
                orderclient.PhoneNumber = item.PhoneNumber;
                orderclient.Email = item.Email;
                orderclient.ClientId = item.Id;
                orderclient.Country = item.Country;
                orderclient.District = item.District;
                orderclient.Address = item.Address + "," + item.City + "," + item.Country;
                orderclient.PostalCode = item.PostalCode;
                orderclient.City = item.City;

                

                intArray[i] = item.Id;
                i++;
                orderclientList.Add(orderclient);
            }
            ViewBag.top  =  intArray;
            

            return View(orderclientList.ToList());
        }
        [Authorize(Roles = "Admin")]
        //[AllowAnonymous]
        [HttpPost]
        public IActionResult AddOrder(string idClient, string address,string idCourier, DateTime date, TimeSpan time)
        {

            
            
            if (ModelState.IsValid)
            {
                Order order = new Order();
                order.Adress = address;
                order.CourierId = idCourier;

                order.ClientId = Convert.ToInt32(idClient);
                order.date = date + time;
                order.Delivery = false;


                _context.order.Add(order);
                _context.SaveChanges();
                return RedirectToAction(nameof(ListOrder));
            }

            return View();
        }
        [Authorize(Roles = "Admin, Courier")]
        public IActionResult Maps()
        {
            return View();
        }
        [Authorize(Roles = "Admin")]
        public IActionResult AddClient()
        {
            return View();
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> AddClient([Bind("Id,Name,Email,Address,PostalCode,City,District,Country,PhoneNumber")] Client client)
        {
            if (ModelState.IsValid)
            {
                _context.client.Add(client);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(ListClients));
            }

            return View();
        }
        [AllowAnonymous]
        public IActionResult DirectionMap()
        {
            var userId = _userManager.GetUserId(User);
            
            ViewBag.TemHoras = false;
            List<Order> TdsOrders = new List<Order>();
            TdsOrders = _context.order.Where(x => x.CourierId == userId).ToList();
            List<string> OrderAddress = new List<string>();
            List<string> OrderTime = new List<string>();

            TdsOrders = TdsOrders.OrderBy(x => x.date).ToList();
            List<int> OrdersIds = new List<int>();
            foreach (var item in TdsOrders)
            {
                if(!item.Delivery)
                {
                    if (item.date.Date == DateTime.Now.Date)
                    {

                        OrderAddress.Add(item.Adress);
                        OrderTime.Add(item.date.ToString("HH:mm"));
                        if (item.date.ToString("HH:mm") != "23:59")
                            ViewBag.TemHoras = true;
                        OrdersIds.Add(item.Id);
                    }
                }
            }
            ViewBag.OrderAddress = OrderAddress;
            ViewBag.OrderTime = OrderTime;
            ViewBag.OrdersIds = OrdersIds;
            List<string> tempos = new List<string>();
            for (int i = 0; i < OrderTime.Count(); i++){
                if(i + 1 < OrderTime.Count())
                    if (OrderTime.Count() > 1  )
                    {
                        var date1 = Convert.ToDateTime(OrderTime[i]);
                        var date2 = Convert.ToDateTime(OrderTime[i + 1]);
                        if(date2.ToString("HH:mm")=="23:59")
                        {
                            tempos.Add("23:59");
                        }
                        else
                        {
                            var result = date2.Subtract(date1);
                            tempos.Add(result.Hours + ":" + result.Minutes);
                        }

                        ViewBag.Horas = tempos;
                    } 
                    else
                    {
                        var date = Convert.ToDateTime(OrderTime[i]);
                        ViewBag.Horas = date.Hour + ":" + date.Minute;
                    }
            }
            if (OrderTime.Count() == 0)
                ViewBag.Horas = tempos;
             
            return View();
        }


        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> EditClient(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cliente = await _context.client.FindAsync(id);
            if (cliente == null)
            {
                return NotFound();
            }
            return View(cliente);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditClient(int id, [Bind("Id,Name,Email,Address,PostalCode,City,District,Country,PhoneNumber")] Client client)
        {
            if (id != client.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(client);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClientExists(client.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(ListClients));
            }
            return View(client);
        }

        private bool ClientExists(int id)
        {
            return _context.client.Any(e => e.Id == id);
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ListClients()
        {
           


            return View(await _context.client.ToListAsync());
        }



        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> EditOrder(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
           
            var order = await _context.order.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            return View(order);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditOrder(int id, [Bind("Id,ClientId,Adress,date,CourierId,Delivery")] Order order)
        {
            if (id != order.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(ListOrder));
            }
            return View(order);
        }

        private bool OrderExists(int id)
        {
            return _context.order.Any(e => e.Id == id);
        }
        [Authorize(Roles = "Admin, Courier")]
        public IActionResult ListOrder(int id)
        {
            var rolesCourier = _context.Roles.FirstOrDefault(y => y.Name == "Courier").Id.ToString();
            var CouriersId = _context.UserRoles.Where(x => x.RoleId == rolesCourier).Select(y => y.UserId).ToList();


            var idc = _userManager.GetUserId(User);
            if(CouriersId.Contains(idc))
            {
                
                List<Order> Lista = new List<Order>();
                foreach (var item in _context.order)
                {
                    if(item.Id==id)
                    {
                        if (item.Delivery)
                            item.Delivery = false;
                        else
                            item.Delivery = true;
                        _context.order.Update(item);
                    }
                    if (item.date.Date == DateTime.Now.Date && item.CourierId == idc)
                    {
                        Lista.Add(item);
                    }
                }
                
                _context.SaveChanges();
                Lista = Lista.OrderBy(x => x.date).ToList();
                return View(Lista);
            }
            else
            {

                return View(_context.order.ToList());
            }

            
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteOrder(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.order
                .FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }
            _context.order.Remove(order);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(ListOrder));

       
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteClient(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var client = await _context.client
                .FirstOrDefaultAsync(m => m.Id == id);
            if (client == null)
            {
                return NotFound();
            }
            _context.client.Remove(client);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(ListClients));
        }

       
    }
}
