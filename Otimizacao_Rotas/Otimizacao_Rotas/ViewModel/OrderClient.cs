﻿using Otimizacao_Rotas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otimizacao_Rotas.ViewModel
{
    public class OrderClient
    {
       

        public Order order { get; set; }

        public int ClientId { get; set; }


        public string Name { get; set; }


       
        public string Email { get; set; }


   
        public string Address { get; set; }


      
        public string PostalCode { get; set; }

   
        public string City { get; set; }

    
        public string District { get; set; }

  
        public string Country { get; set; }


   
        public int PhoneNumber { get; set; }


        public string CourierId { get; set; }
        
        public string CourierName { get; set; }
    }
}
